项目名：SimpleMybatisMysql
-- 这是Mybatis操作mysql数据库的配置文件版本实现。

环境要求：
jdk -- 这里是 jdk1.8
maven -- 这里采用maven管理源码 maven 3.6.0
mysql -- 这里 8.0.13版本

开发工具： 这里用的是 IDEA

依赖的jar包：
查看 pom.xml

用到的技术：
Mybatis是对 mysql-connector-java 的进一步封装。
mysql-connector-java 是 java 使用 mysql 的包。

参考我的博客：https://www.cnblogs.com/zhangchao0515/p/14312915.html

框架:Mybatis+mysql入门使用
一、简介
　　MyBatis 是一款优秀的持久层框架，它支持定制化 SQL、存储过程以及高级映射。

二、入门使用
　　1、添加依赖
        a、使用maven管理依赖，以我本次使用的版本为例
        b、也可以使用gradle管理依赖
        c、也可以去官网下载，官网地址：http://www.mybatis.org/mybatis-3/
    2、初始化数据库和初始数据，以数据库moy_mybatis和表t_test为例
    3、新建实体类TestEntity
    4、新建接口TestMapper
    5、新建实体映射文件Test.xml，这里的namespace就是写TestMapper全类名，否则不能使用动态代理的方式调用接口TestMapper
    6、新建mybatis配置信息文件mybatis-config.xml
    7、为了方便测试，编写一个获取SqlSession的工具类Mybatis3Utils
    8、新建测试类TestMapperTest测试

其他参考资料：
框架:Mybatis+mysql入门使用
https://www.cnblogs.com/moy25/p/8455252.html

【Mybatis】MyBatis之配置多数据源（十）:
https://www.cnblogs.com/h--d/p/11304082.html

MyBatis多数据源配置(读写分离)
https://blog.csdn.net/isea533/article/details/46815385

Mybatis自定义DataSource使用druid:
https://blog.csdn.net/liyantianmin/article/details/83722383

MyBatis在非Spring环境下第三方DataSource设置-Druid篇:
https://www.cnblogs.com/ChenJunHacker/p/6401797.html

SpringMVC简介及第一个HelloWorld:
https://blog.csdn.net/evankaka/article/details/45501811

Mybatis - 解决实体类属性和数据库列名不对应的两种方式：
https://blog.csdn.net/Yuz_99/article/details/89576357

MyBatis——resultMap实体类名和列名的对应：
https://www.cnblogs.com/qq2210446939/p/13540758.html

Mybatis对象生成与属性赋值-反射技术:
https://blog.csdn.net/Aqu415/article/details/103227583?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control

Java取得一个对象里所有get方法和set方法, 读取某个类下所有变量的名称:
https://www.cnblogs.com/aimei/archive/2004/01/13/12721522.html

Java反射之——Java获取类的成员函数的信息
https://blog.csdn.net/qq_42296486/article/details/84782745

搭建Mybatis+Oracle项目以及简单的增删改查语法：
https://www.imooc.com/article/details/id/22088

SpringBoot配置Mybatis的两种方式（通过XML配置和通过YML配置文件配置）
https://blog.csdn.net/weixin_43966635/article/details/112342116

mybatis使用Resources读取配置文件
https://blog.csdn.net/xiaozaq/article/details/54173807

关于mybatis两种连接mysql的方式（注解和xml配置）
https://blog.csdn.net/d007letian/article/details/78100260

-- 2、初始化数据库和初始数据，以数据库moy_mybatis和表t_test为例

-- 创建数据库
CREATE SCHEMA IF NOT EXISTS `db2021` Character Set utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `db2021`;

--  创建一个测试表
CREATE TABLE IF NOT EXISTS `db2021`.`t_test`(
    id INT(11) AUTO_INCREMENT,
    create_time DATE COMMENT '创建时间',
    modify_time DATE COMMENT '修改时间',
    content VARCHAR (50) COMMENT '内容',
    PRIMARY KEY (id)
)ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

INSERT INTO `t_test`(`id`, `create_time`, `modify_time`, `content`) VALUES (1, TIMESTAMP, null, '我是内容');
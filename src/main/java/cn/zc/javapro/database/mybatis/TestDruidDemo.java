package cn.zc.javapro.database.mybatis;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.Reader;

public class TestDruidDemo {
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        String resource = "mybatis-config.xml";
        // 使用类加载器加载mybatis的配置文件（它也加载关联的映射文件）
        Reader reader = Resources.getResourceAsReader(resource);

        // 构建sqlSession的工厂
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder()
                .build(reader,"development");

        SqlSession session = sessionFactory.openSession();

        System.out.println(session);

        session = sessionFactory.openSession();

        System.out.println(session);

        DruidDataSourceFactory f;
    }
}

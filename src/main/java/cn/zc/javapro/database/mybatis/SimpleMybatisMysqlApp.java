package cn.zc.javapro.database.mybatis;

import cn.zc.javapro.database.mybatis.entity.UserEntity;
import cn.zc.javapro.database.mybatis.mapper.TestMapper;
import cn.zc.javapro.database.mybatis.mapper.TestMapper2;
import cn.zc.javapro.database.mybatis.mapper.UserMapper;
import cn.zc.javapro.database.mybatis.tools.MybatisTools;
import cn.zc.javapro.database.mybatis.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;

public class SimpleMybatisMysqlApp {
    public static void main(String[] args) {
        System.out.println("------------test3--------------");
        test3();
        System.out.println("------------test4--------------");
        test4();
    }

    public static void test4() {
        SqlSession sqlSession;
        UserMapper userMapper;
        //
        MybatisTools mybatisTools = new MybatisTools("mybatis-mysql-config.xml");
        sqlSession = mybatisTools.openCurrentSqlSession();
        //得到 UserMapper 接口的实现类对象，UserMapper 接口的实现类对象由 sqlSession.getMapper(UserMapper.class)动态构建出来
        userMapper = sqlSession.getMapper(UserMapper.class);

        //执行查询操作，将查询结果自动封装成 UserEntity 返回
        for(UserEntity userEntity: userMapper.list()) {
            System.out.println(userEntity);
        }

        mybatisTools.closeCurrentSqlSession();
    }

    public static void test3() {
        SqlSession sqlSession;
        TestMapper2 testMapper2;
        //
        MybatisTools mybatisTools = new MybatisTools("mybatis-config.xml");
        sqlSession = mybatisTools.openCurrentSqlSession();
        //得到 TestMapper 接口的实现类对象，TestMapper 接口的实现类对象由 sqlSession.getMapper(TestMapper.class)动态构建出来
        testMapper2 = sqlSession.getMapper(TestMapper2.class);

        //执行查询操作，将查询结果自动封装成 TestEntity 返回
        System.out.println(testMapper2.get(1));

        mybatisTools.closeCurrentSqlSession();
    }

    public static void test2() {
        SqlSession sqlSession;
        TestMapper testMapper;
        //
        MybatisTools mybatisTools = new MybatisTools("mybatis-config.xml");
        sqlSession = mybatisTools.openCurrentSqlSession();
        //得到 TestMapper 接口的实现类对象，TestMapper 接口的实现类对象由 sqlSession.getMapper(TestMapper.class)动态构建出来
        testMapper = sqlSession.getMapper(TestMapper.class);

        //执行查询操作，将查询结果自动封装成 TestEntity 返回
        System.out.println(testMapper.get(1));

        mybatisTools.closeCurrentSqlSession();
    }

    public static void test() {
        SqlSession sqlSession;
        TestMapper testMapper;
        sqlSession = MybatisUtils.getCurrentSqlSession();
        testMapper = sqlSession.getMapper(TestMapper.class);

        //
        System.out.println(testMapper.get(1));

        MybatisUtils.closeCurrentSession();
    }

}

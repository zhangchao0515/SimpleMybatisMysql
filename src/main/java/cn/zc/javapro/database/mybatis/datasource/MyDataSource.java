package cn.zc.javapro.database.mybatis.datasource;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.apache.ibatis.datasource.DataSourceFactory;

import javax.sql.DataSource;
import java.util.Properties;

public class MyDataSource extends DruidDataSourceFactory implements DataSourceFactory {
    protected Properties properties;

    @Override
    public void setProperties(Properties properties) {
        this.properties= properties;
    }

    @Override
    public DataSource getDataSource() {
        try {
            return createDataSource(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

package cn.zc.javapro.database.mybatis.mapper;

import cn.zc.javapro.database.mybatis.entity.UserEntity;

import java.util.List;

public interface UserMapper {

    List<UserEntity> list();
}

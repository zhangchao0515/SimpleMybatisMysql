package cn.zc.javapro.database.mybatis.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * [Project]:
 * [Date]:2021/1/22
 * [Description]: java mybatis mysql maven
 * @author zc
 */
//让它实现序列化接口
@Getter
@Setter
@ToString
public class TestEntity implements Serializable {
    private Integer id;
    private Date createTime;
    private Date modifyTime;
    private String content;
}

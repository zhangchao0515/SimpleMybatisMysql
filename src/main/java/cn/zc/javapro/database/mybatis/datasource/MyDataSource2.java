package cn.zc.javapro.database.mybatis.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.datasource.unpooled.UnpooledDataSourceFactory;

import javax.sql.DataSource;

public class MyDataSource2 extends UnpooledDataSourceFactory {
    protected DataSource dataSource;

    public MyDataSource2() {
        this.dataSource = new DruidDataSource();

    }
}

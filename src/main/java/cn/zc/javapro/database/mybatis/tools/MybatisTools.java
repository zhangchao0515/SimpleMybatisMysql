package cn.zc.javapro.database.mybatis.tools;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;
import java.util.Objects;

/**
 * [Project]:
 * [Email]:
 * [Date]:2021/1/22
 * [Description]:
 * 7、为了方便测试，编写一个获取SqlSession的工具类MybatisTools
 * 该类主要是加载mybatis的配置文件（如：mybatis-config.xml等），然后
 *
 * 使用的时候:
 * 1）先 MybatisTools mt = new MybatisTools(resourceXMLFile)  // 这里的resourceXMLFile是mybatis的配置文件，如"mybatis-config.xml"
 * 2) SqlSession sqlSession = mt.openCurrentSqlSession()  // 按照配置文件打开获取 SqlSession
 * 3) TestMapper testMapper = sqlSession.getMapper(TestMapper.class); // 这里的 TestMapper.class 是实体和数据库的交互接口。
 * 4) testMapper.getAll()  // 调用方法
 * 5) mt.closeCurrentSqlSession() // 关闭 SqlSession
 * @author zc
 */
public class MybatisTools {
    //private String resourceFile;
    private ExecutorType executorType;
    private SqlSessionFactory sqlSessionFactory;
    private ThreadLocal<SqlSession> sessionThread = new ThreadLocal<>();

    /**
     *
     * @param resourceFile 资源文件
     */
    public MybatisTools(String resourceFile) {
        //this.resourceFile = resourceFile;
        this.initSqlSessionFactory(resourceFile);
    }

    /**
     *
     * @param resourceFile
     * @param executorType
     */
    public MybatisTools(String resourceFile, ExecutorType executorType) {
        //this.resourceFile = resourceFile;
        this.initSqlSessionFactory(resourceFile);
        this.executorType = executorType;
    }

    private void initSqlSessionFactory(String resourceFile) {
        try {
            Reader reader = Resources.getResourceAsReader(resourceFile);
            // 构建sqlSession的工厂
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public SqlSession openCurrentSqlSession() {
        SqlSession sqlSession = sessionThread.get();
        if (Objects.isNull(sqlSession)) {
            if(this.executorType == null) {
                sqlSession = sqlSessionFactory.openSession();
            } else {
                // 通过session设置ExecutorType开启批量添加，类似jdbc的addBatch操作
                //sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
                sqlSession = sqlSessionFactory.openSession(this.executorType);
            }

            sessionThread.set(sqlSession);
        }
        return sqlSession;
    }

    public void commitCurrentSqlSession() {
        SqlSession sqlSession = sessionThread.get();
        if (Objects.nonNull(sqlSession)) {
            sqlSession.commit();
        }
    }

    public void closeCurrentSqlSession() {
        SqlSession sqlSession = sessionThread.get();
        if (Objects.nonNull(sqlSession)) {
            /*if (this.executorType != null) {
                sqlSession.commit();
            }*/
            sqlSession.close();
        }
        sessionThread.set(null);
    }
}

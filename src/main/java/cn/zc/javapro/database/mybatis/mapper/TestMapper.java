package cn.zc.javapro.database.mybatis.mapper;

import cn.zc.javapro.database.mybatis.domain.QueryVo;
import cn.zc.javapro.database.mybatis.entity.TestEntity;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * [Project]:
 * [Email]:
 * [Date]:2021/1/22
 * [Description]:
 *
 * @author zc
 */
public interface TestMapper {
    List<TestEntity> list();

    TestEntity get(Serializable id);
    //TestEntity get(Integer id);
    //TestEntity get(@Param("id") Integer id);

    int insert(TestEntity TestEntity);

    int update(TestEntity TestEntity);

    int delete(Serializable id);

    int count();

    /**
     * 根据名称模糊查询信息
     */
    List<TestEntity> findByContent(String content);

    /**
     * 根据queryVo中的条件查询用户
     */
    List<TestEntity> findTestByVo(QueryVo vo);
}

package cn.zc.javapro.database.mybatis.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserEntity implements Serializable {
    private int id;
    // 邮箱
    private String email;
    // 昵称
    private String nickName;
    // 密码
    private String passWord;
    // 注册时间
    private String regTime;
    // 用户名
    private String userName;
}

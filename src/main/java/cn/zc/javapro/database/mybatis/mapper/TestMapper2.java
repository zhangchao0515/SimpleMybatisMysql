package cn.zc.javapro.database.mybatis.mapper;

import cn.zc.javapro.database.mybatis.domain.QueryVo;
import cn.zc.javapro.database.mybatis.entity.TestEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.io.Serializable;
import java.util.List;

/**
 * [Project]:
 * [Email]:
 * [Date]:2021/1/22
 * [Description]:
 *
 * @author zc
 */
public interface TestMapper2 {
    @Select("select id, create_time as createTime, modify_time as modifyTime, content from t_test;")
    List<TestEntity> list();

    @Select("select id, create_time as createTime, modify_time as modifyTime, content from t_test where id = #{id};")
    TestEntity get(Serializable id);
    //TestEntity get(Integer id);
    //TestEntity get(@Param("id") Integer id);
    @Insert("INSERT INTO t_test (create_time,modify_time,content)\n" +
            "        VALUES (#{createTime},#{modifyTime},#{content})")
    int insert(TestEntity TestEntity);

    @Update("UPDATE t_test SET modify_time=#{modifyTime},content=#{content}\n" +
            "        WHERE id = #{id}")
    int update(TestEntity TestEntity);

    @Delete("DELETE FROM t_test WHERE id = #{id}")
    int delete(Serializable id);

    @Select("SELECT count(*) FROM t_test")
    int count();

    /**
     * 根据名称模糊查询信息
     */
    @Select("select id, create_time as createTime, modify_time as modifyTime, content\n" +
            "         from t_test where content like '%${content}%'")
    List<TestEntity> findByContent(String content);

    /**
     * 根据queryVo中的条件查询用户
     */
    @Select("select id, create_time as createTime, modify_time as modifyTime, content\n" +
            "         from t_test where id like #{testEntity.id};")
    List<TestEntity> findTestByVo(QueryVo vo);
}

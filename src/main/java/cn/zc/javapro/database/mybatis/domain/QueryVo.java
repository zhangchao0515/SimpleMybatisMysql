package cn.zc.javapro.database.mybatis.domain;

import cn.zc.javapro.database.mybatis.entity.TestEntity;

public class QueryVo {
    private TestEntity testEntity;

    public TestEntity getTestEntity() {
        return testEntity;
    }

    public void setTestEntity(TestEntity testEntity) {
        this.testEntity = testEntity;
    }
}

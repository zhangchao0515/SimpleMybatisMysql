package cn.zc.javapro.database.mybatis.mapper;

import cn.zc.javapro.database.mybatis.domain.QueryVo;
import cn.zc.javapro.database.mybatis.entity.TestEntity;
import cn.zc.javapro.database.mybatis.tools.MybatisTools;
import cn.zc.javapro.database.mybatis.utils.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TestMapperTest {
    MybatisTools mybatisTools;
    SqlSession sqlSession;
    TestMapper testMapper;

    @Before
    public void before() {
        /*sqlSession = MybatisUtils.getCurrentSqlSession();
        testMapper = sqlSession.getMapper(TestMapper.class);*/
        mybatisTools = new MybatisTools("mybatis-config.xml");
        sqlSession = mybatisTools.openCurrentSqlSession();
        testMapper = sqlSession.getMapper(TestMapper.class);
    }

    @After
    public void after() {
        // MybatisUtils.closeCurrentSession();
        mybatisTools.closeCurrentSqlSession();
    }

    @Test
    public void insert() {
        TestEntity entity = new TestEntity();
        entity.setCreateTime(new Date());
        entity.setModifyTime(new Date());
        entity.setContent("我是内容");
        //entity.setaontent("我是内容");
        System.out.println(testMapper.insert(entity));
        sqlSession.commit();
    }

    @Test
    public void count() {
        System.out.println(testMapper.count());
    }

    @Test
    public void list() {
        List<TestEntity> list = testMapper.list();
        //System.out.println(Arrays.toString(list.toArray()));
        for (TestEntity testEntity : list) {
            System.out.println(testEntity);
        }
    }

    @Test
    public void get() {
        System.out.println(testMapper.get(1));
    }

    @Test
    public void update() {
        TestEntity entity = new TestEntity();
        entity.setId(1);
        entity.setModifyTime(new Date());
        entity.setContent("我是修改后内容");
        //entity.setaontent("我是修改后内容");
        testMapper.update(entity);
        sqlSession.commit();
    }

    @Test
    public void delete() {
        testMapper.delete(1);
        sqlSession.commit();
    }

    /**
     * 测试模糊查询操作
     */
    @Test
    public void findByContent(){
        List<TestEntity> testEntityList = testMapper.findByContent("内容");
        for(TestEntity testEntity: testEntityList){
            System.out.println(testEntity);
        }
    }

    /**
     * 测试使用QueryVo作为查询对象
     */
    @Test
    public void testFindByVo(){
        QueryVo vo = new QueryVo();
        TestEntity testEntity = new TestEntity();
        //testEntity.setContent("%内容%");
        testEntity.setId(1);
        vo.setTestEntity(testEntity);
        List<TestEntity> testEntityList = testMapper.findTestByVo(vo);
        for(TestEntity testEntity1 : testEntityList){
            System.out.println(testEntity1);
        }

    }
}
